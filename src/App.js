import logo from "./logo.svg";
import "./App.css";
import Glasses_Tryon from "./Glasses_Tryon/Glasses_Tryon";

function App() {
  return (
    <div className="App">
      <Glasses_Tryon />
    </div>
  );
}

export default App;
